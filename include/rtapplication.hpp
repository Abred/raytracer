#ifndef __RTAPPLICATION_HPP__
#define __RTAPPLICATION_HPP__

#include "light.hpp"
#include "shape.hpp"
#include "material.hpp"
#include "camera.hpp"

#include "glutwindow.hpp"
#include "ppmwriter.hpp"
#include "pixel.hpp"
#include "vector.hpp"
#include "rgb.hpp"
#include "scene.hpp"

#include <iostream>
#include <cmath>
#include <list>
#include <sys/time.h>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>



class rtApplication 
{

	//Konstruktoren,Destruktor	
	public :
 
						rtApplication();
						rtApplication(std::list<shape*> const&, camera const&, std::list<light> const&);
						rtApplication(scene const&);
						~rtApplication();

	//Memberfunktionen
	public:
			void 			raytrace() 				const;
			void 			raytrace1() 				const;
			void 			raytrace2() 				const;
			void 			raytrace3() 				const;
			rgb			rraytrace(ray  , int)			const;
			rgb			shade(ray,shape*,double)		const;
			rgb			shade(ray ,shape*,math3d::point, int)	const;
			

	private :

			std::list<shape*>	shapeList_;
			camera 			cam_;
			std::list<light>	lightList_;
			bool			antia_;
			std::string		picname_;
};



#endif /* __RTAPPLICATION_HPP__ */

