#ifndef _SPHERE_HPP
#define	_SPHERE_HPP

#include "shape.hpp"
#include "vector.hpp"
#include "point.hpp"
#include "rgb.hpp"
#include "ray.hpp"
#include "matrix.hpp"
#include "material.hpp"

#include <limits>
#include <cmath>

class sphere : public shape
{

	//Konstruktoren
	public:
                        				sphere();
							sphere(math3d::point const&, double, std::string const&, material const&, 
								math3d::matrix const& matr    = math3d::matrix::identity(), 
								math3d::matrix const& matrInv = math3d::matrix::identity()  );
							sphere(sphere const&);
							~sphere();

	//Memberfunktionen
	public:
 
			/*virtual*/ 	double		volume()								const;
			/*virtual*/ 	bool		isInside(math3d::point const&)						const;
			/*virtual*/	double		surface()								const;
			/*virtual*/	void		printOn( std::ostream&)							const;
			/*virtual*/	sphere*		clone()									const;
			/*virtual*/	int		intersection(ray const&, double &, math3d::point &)			const;
			/*virtual*/	math3d::vector	getNormal(math3d::point const&)						const;
					int		checkIsPoint(ray const&, ray const&, double, double &, math3d::point &)	const;

	private:
					math3d::point	center_;
					double 		radius_;

};



#endif	/* _sphere_HPP */

