#ifndef _SHAPE_HPP
#define	_SHAPE_HPP

#include "ray.hpp"
#include "material.hpp"
#include "transform.hpp"

#include "matrix.hpp"
#include "rgb.hpp"
#include "point.hpp"

#include <iostream>
#include <string>

class shape
{

	//Konstruktoren
	public:
	
	                      			  shape();
						shape(std::string const&, material const&, 
								math3d::matrix const& matr,     
								math3d::matrix const& matrInv );
						shape(shape const&);
			virtual			~shape();

	//Memberfunktionen
	public:
 
			virtual double		volume() 							const 	=0;
			virtual bool		isInside(math3d::point const&)					const	=0;
			virtual	double		surface()							const	=0;
			virtual shape*		clone()								const	=0;
			virtual	int		intersection(ray const&, double &, math3d::point &)		const	=0;
			virtual math3d::vector	getNormal(math3d::point const&)					const	=0;
				std::string	getName()							const;
				material	getMaterial()							const;
				math3d::matrix const&	getWorldMatrix()					const;
				math3d::matrix const&	getWorldMatrixInvers()					const;
			virtual	void		printOn(std::ostream& = std::cout)				const;
				int		ref();
				int		unRef();
				void		translate(math3d::vector const&);
				void		scale(math3d::vector const&);
				void		rotate(math3d::vector const&, double);
				

	//Membervariablen
	private:

				std::string	name_;
				int		refCount_;
				material	material_;
				math3d::matrix	worldMatrix_;
				math3d::matrix	worldMatrixInvers_;

};

std::ostream&	operator<<(std::ostream&, shape const&);
bool		operator==(shape const& lhs, shape const& rhs);



#endif	/* _shape_HPP */

