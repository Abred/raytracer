#ifndef _CONE_HPP
#define	_CONE_HPP

#include "shape.hpp"
#include "matrix.hpp"
#include "vector.hpp"
#include "point.hpp"
#include "rgb.hpp"
#include "ray.hpp"
#include "material.hpp"

#include <limits>
#include <cmath>


class cone : public shape
{

	//Konstruktoren
	public:
                        			cone();
						cone( math3d::point const&, double, double, std::string const&, material const&,
							math3d::matrix const& matr    = math3d::matrix::identity(), 
							math3d::matrix const& matrInv = math3d::matrix::identity()  );
						cone(cone const&);
						~cone();
	//Memberfunktionen
	public:
 
		/*virtual*/ 	double		volume()						const;
		/*virtual*/ 	bool		isInside(math3d::point const&)				const;
		/*virtual*/	double		surface()						const;
		/*virtual*/	void		printOn(std::ostream&)					const;
		/*virtual*/	cone*		clone()							const;
		/*virtual*/	int		intersection(ray const&, double &, math3d::point &)	const;
		/*virtual*/	math3d::vector	getNormal(math3d::point const&)				const;
		/*virtual*/	math3d::vector	getNormal(math3d::point const&, double p)		const;



	//Membervariablen
	private:
				math3d::point	center_;
				double		height_;
				double		radius_;
};



#endif	/* _CONE_HPP */
