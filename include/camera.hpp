#ifndef __CAMERA_HPP__
#define __CAMERA_HPP__


#include "matrix.hpp"
#include "transform.hpp"
#include "vector.hpp"


#include <string>

class camera
{
	//Konstruktoren
	public:
					camera();
					camera(std::string const&, double,
						math3d::matrix const& matr    = math3d::matrix::identity(), 
						math3d::matrix const& matrInv = math3d::matrix::identity() );
					~camera();

	//Memberfunktionen
	public:
			void		translate(math3d::vector const&);
			void		scale(math3d::vector const&);
			void		rotate(math3d::vector const&, double);
			std::string	get_name()				const;
			double		get_angle()				const;
			math3d::matrix	getWorldMatrix()			const;
			math3d::matrix	getWorldMatrixInvers()			const;

	//Membervariablen
	private:

			std::string	name_;
			double		fovX_;
			math3d::matrix	worldMatrix_;
			math3d::matrix	worldMatrixInvers_;
};


#endif /* __camera_HPP__ */
