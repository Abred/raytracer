#ifndef __SCENE_PARSER__
#define __SCENE_PARSER__

#include "shape.hpp"
#include "sphere.hpp"
#include "cone.hpp"
#include "cylinder.hpp"
#include "triangle.hpp"
#include "box.hpp"
#include "material.hpp"
#include "camera.hpp"
#include "light.hpp"


#include<list>
#include<string>
#include<fstream>
#include<sstream>

struct scene

{
	//Konstruktor
	
					scene(std::string const&);
					~scene();
	

	//Membervariablen

		std::list<shape*>	objects_;
		std::list<light>	lights_;
		std::list<material>	mats_;
		std::list<camera>	cams_;
		camera			cam_;
		double 			x_res_;
		double			y_res_;
		std::string		filename_;
		bool			antialiasing_;

};

#endif /*_SCENE_PARSER_*/
