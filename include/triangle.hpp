#ifndef _TRIANGLE_HPP
#define	_TRIANGLE_HPP

#include "shape.hpp"
#include "matrix.hpp"
#include "vector.hpp"
#include "point.hpp"
#include "rgb.hpp"
#include "ray.hpp"
#include "material.hpp"


#include <cmath>


class triangle : public shape
{

	//Konstruktoren
	public:
                        				triangle();
							triangle( math3d::point const&, math3d::point const&,math3d::point const&,
									std::string const&, material const&,
									math3d::matrix const& matr    = math3d::matrix::identity(), 
									math3d::matrix const& matrInv = math3d::matrix::identity()  );
							triangle(triangle const&);
							~triangle();
 	//Memberfunktionen
	public:
			/*virtual*/ 	double		volume()						const;
			/*virtual*/ 	bool		isInside(math3d::point const&)				const;
			/*virtual*/	double		surface()						const;
			/*virtual*/	void		printOn(std::ostream&)					const;
			/*virtual*/	triangle	*clone()						const;
			/*virtual*/	int		intersection(ray const&, double &, math3d::point &)	const;
			/*virtual*/	math3d::vector	getNormal(math3d::point const&)				const;



	//Membervariablen
	private:
					math3d::point	p1_;
					math3d::point	p2_;
					math3d::point	p3_;
};



#endif	/* _Triangle_HPP */
