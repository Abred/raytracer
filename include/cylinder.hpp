#ifndef _CYLINDER_HPP
#define	_CYLINDER_HPP

#include "shape.hpp"
#include "matrix.hpp"
#include "vector.hpp"
#include "point.hpp"
#include "rgb.hpp"
#include "ray.hpp"
#include "material.hpp"


#include<cmath>

class cylinder : public shape
{

	//Konstruktoren
	public:
							cylinder();
							cylinder(math3d::point const&,double,double, std::string const&, material const&, 
									math3d::matrix const& matr	=math3d::matrix::identity(),
									math3d::matrix const& matrInv	=math3d::matrix::identity()	);
							cylinder(cylinder const&);
							~cylinder();

	//Memberfunktionen
	public:

			/*virtual*/ 	double		volume()						const;
			/*virtual*/ 	bool		isInside(math3d::point const&)				const;
			/*virtual*/	double		surface()						const;
			/*virtual*/	void		printOn(std::ostream&)					const;
			/*virtual*/	cylinder*	clone()							const;
			/*virtual*/	int		intersection(ray const&, double &, math3d::point &)	const;
			/*virtual*/	math3d::vector	getNormal(math3d::point const&,double)			const;
			/*virtual*/	math3d::vector	getNormal(math3d::point const&)				const;


	//Membervariablen
	private:

					math3d::point	center_;
					double		radius_;
					double		height_;


};

#endif /*_CYLINDER_HPP*/
