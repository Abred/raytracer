#ifndef __RAY_HPP__
#define __RAY_HPP__

#include "light.hpp"
#include "vector.hpp"

#include <cmath>



class ray
{

	//Konstruktoren
	public:

							ray();
							ray(math3d::point a, math3d::vector b);
							ray(math3d::point a, math3d::vector b, double);
	//getter					
	public:
					math3d::point	get_origin()		const;
					math3d::vector	get_direction()		const;

	//Membervariablen
	private:
		
					math3d::point	origin_;
					math3d::vector	direction_;
};


#endif /* __RAY_HPP__ */

