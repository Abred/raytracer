

#include "sphere.hpp"

//
// Konstruktoren
//
sphere::sphere() 	
  {}


sphere::sphere(math3d::point const& c, double r, std::string const& name, material const& mat, math3d::matrix const& matr, math3d::matrix const& matrInv) : 
		shape(name, mat, matr, matrInv) , 
		center_(c) , 
		radius_(r)	
//Sphere "normalisieren"	
{
	if (radius_ != 1)
	{
		scale ( math3d::vector(radius_,radius_,radius_) );
		radius_ = 1;
	}
	

	if (center_[math3d::point::x] != 0 || center_[math3d::point::y] != 0 || center_[math3d::point::z] != 0  )
	{
		translate ( math3d::vector(center_[math3d::point::x], center_[math3d::point::y], center_[math3d::point::z] ));
		center_ = math3d::point (0,0,0);	
	}
}

sphere::sphere(sphere const& sp) : 
		shape(sp) , 
		center_(sp.center_) , 
		radius_(sp.radius_) 	
  {}

sphere::~sphere() 	
  {}
//


//
// volume - Berechnung Kugelvolumen
//
double	sphere::volume()	const
{
	return pow(radius_,3)*4/3*M_PI;
}
//


//
// isInside - Kollision
//
bool	sphere::isInside(math3d::point const& p)	const
{
	return ( (sqrt(pow(center_[math3d::point::x]-p[math3d::point::x],2) + pow(center_[math3d::point::y]-p[math3d::point::y],2) + pow(center_[math3d::point::z]-p[math3d::point::z],2))) <= radius_);
}
//


//
// surface - Berechnung Oberfläche
//
double	sphere::surface()	const
{
	return 4* radius_ * radius_ * M_PI; 
}
//


//
// printOn - Ausgabe
//
void sphere::printOn(std::ostream& stream)		const
{
	stream << "\nSphere:\n";
	shape::printOn(stream);
	stream << "Mittelpunkt: (" << center_[math3d::point::x] << "," << center_[math3d::point::y] << "," << center_[math3d::point::z] << ")\n";
	stream << "Radius: " << radius_ << "\n";
}
//


//
// clone
//
sphere* sphere::clone()	const
{
	return new sphere(*this);	
}
//


//
// intersection
//
int sphere::intersection(ray const& ry, double & t, math3d::point & p)		const
{
	//Strahl transformieren
	//Multiplikation mit inverser Matrix der Sphere
	ray rayTransformed (getWorldMatrixInvers() * ry.get_origin() , getWorldMatrixInvers() * ry.get_direction(), 1.0 );

	//Kugelgleichung, im Ursprung, Radius 1
	//x*x - 1 = 0 -> allg. Geradenpunkt einsetzen, lambda berechnen
	double tmpB = rayTransformed.get_origin()[math3d::point::x] * rayTransformed.get_direction()[math3d::vector::x] + 
		      rayTransformed.get_origin()[math3d::point::y] * rayTransformed.get_direction()[math3d::vector::y] +
 		      rayTransformed.get_origin()[math3d::point::z] * rayTransformed.get_direction()[math3d::vector::z];

	double tmpC = rayTransformed.get_origin()[math3d::point::x] * rayTransformed.get_origin()[math3d::point::x] + 
		      rayTransformed.get_origin()[math3d::point::y] * rayTransformed.get_origin()[math3d::point::y] +
 		      rayTransformed.get_origin()[math3d::point::z] * rayTransformed.get_origin()[math3d::point::z];   

	double a = math3d::dot(rayTransformed.get_direction(), rayTransformed.get_direction());
	double b = 2 * tmpB;
	double c = tmpC - radius_*radius_;

	double under_sqrt = (b * b) - (4 * a * c);

	if(under_sqrt < 0)
	{
		return 0;
	}
	else if (under_sqrt > 0 && (under_sqrt > std::numeric_limits<double>::epsilon()) )
	{
		double i_1 = (-b - sqrt(under_sqrt)) / (2 * a);
		double i_2 = (-b + sqrt(under_sqrt)) / (2 * a);

		if( i_1 <= i_2 && i_1 > 0)
		{
			return checkIsPoint (ry, rayTransformed, i_1, t, p);
		}
		else if (i_2 > 0)
		{
			return checkIsPoint (ry, rayTransformed, i_2, t, p);
		}
		else
			return 0;	
	}
	else
	{
		double i = -b / (2 * a);

		if (i > 0)
		{
			return checkIsPoint (ry, rayTransformed, i, t, p);	
		}
		else
			return 0;
	}
}
//


//
// getNormal
//
math3d::vector	sphere::getNormal(math3d::point const& p)	const
{
	math3d::point pTransformed (getWorldMatrixInvers() * p);

	return math3d::normalize (math3d::transpose(getWorldMatrixInvers()) * (pTransformed-center_) );
}
//


//
// checkIsPoint
//
int sphere::checkIsPoint(ray const& ry, ray const& rayTransformed, double i, double & t, math3d::point & p)	const
{
	math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i * rayTransformed.get_direction());
	double l = math3d::length (pNeu - ry.get_origin());		

	if (l < t)
	{
		p = pNeu;
		t = l;
		if (math3d::length (center_ - (rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction())) > 1)
			return 1;		
		else 
			return -1;
	}
	else
		return 0;
}



