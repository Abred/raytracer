#include "cone.hpp"

//
// Konstruktoren, Destruktor
//
cone::cone() 	
  {}

cone::cone(math3d::point const& cntr, double h, double r, std::string const& name, material const& mat, math3d::matrix const& matr, math3d::matrix const& matrInv) : 
		shape(name,mat, matr, matrInv) , 
		center_ (cntr), 
		height_ (h),	
		radius_ (r)
//Kegel "normalisieren"	
{
	if (radius_ != 1)
	{
		scale ( math3d::vector(radius_,1,radius_) );
		radius_ = 1;
	}
	
	if (height_ != 1)
	{
		scale (math3d::vector(1,height_,1) );
		height_ = 1;
	}
	if (center_[math3d::point::x] != 0 || center_[math3d::point::y] != 0 || center_[math3d::point::z] != 0  )
	{
		translate ( math3d::vector(center_[math3d::point::x], center_[math3d::point::y], center_[math3d::point::z] ));
		center_ = math3d::point (0,0,0);	
	}

}

cone::cone(cone const& cn) : 
		shape(cn) , 
		center_(cn.center_) , 
		height_ (cn.height_),
		radius_ (cn.radius_)		
  {}

cone::~cone() 	
  {}


//
// volume - Berechnung conevolumen
//
double	cone::volume()	const
{
	return  std::abs( 0.0  );
}



//
// isInside - Kollision
//
bool	cone::isInside(math3d::point const& p)	const
{
	return ( 0 );
}


//
// surface - Berechnung Oberfläche
//
double	cone::surface()	const
{
     return (  std::abs( 0.0 ) );
}


//
// printOn - Ausgabe
//
void cone::printOn(std::ostream& stream)		const
{
	stream << "\nCone: \n";
	shape::printOn(stream);
	stream << "Mittelpunkt: " << center_[math3d::point::x] << " " 
		<< center_[math3d::point::y] << " " << center_[math3d::point::z] << "\n";
	stream << "Hoehe, Radius: " << height_ << ", " 
		<< radius_ << "\n";
	stream << "Volumen: " << volume() << "\nOberfläche: " << surface() << "\n";
}
 


//
// clone
//
cone* cone::clone()	const
{
	return new cone(*this);	
}


//
// intersection
//
int cone::intersection(ray const& ry, double & t, math3d::point & p) const
{
	//Strahl transformieren
	//Multiplikation mit inverser Matrix des Cone

	ray rayTransformed = ray(getWorldMatrixInvers() * ry.get_origin() , getWorldMatrixInvers() * ry.get_direction(),1.0);

	int inout = 0;
	double talt = t;
	double i_3 = -rayTransformed.get_origin()[math3d::point::y] / rayTransformed.get_direction()[math3d::vector::y];

	if (i_3 > 0)
	{
		math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i_3 * rayTransformed.get_direction() );
		math3d::point pTest = rayTransformed.get_origin() + i_3 * rayTransformed.get_direction() ;
		double l = math3d::length (pNeu - ry.get_origin());		

		if (l < t && math3d::length(pTest - center_) < 1.0)
		{
			p = pNeu;
			t = l;
			math3d::point pp(rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction());
			if ( pp[math3d::point::y] < 0  )
				inout = 1;
			else 
				inout = -1;
		}	
	}


	double p1 = rayTransformed.get_origin()[math3d::point::x];
	double p2 = rayTransformed.get_origin()[math3d::point::y];
	double p3 = rayTransformed.get_origin()[math3d::point::z];

	double v1 = rayTransformed.get_direction()[math3d::vector::x];
	double v2 = rayTransformed.get_direction()[math3d::vector::y];
	double v3 = rayTransformed.get_direction()[math3d::vector::z];


	double a = (v1 * v1) - (v2 * v2) + (v3 * v3);
	double b = 2* ((p1 * v1) - (p2 * v2) + (p3 * v3) + v2);
	double c = (p1 * p1) - (p2 * p2) + (p3 * p3) + (2 * p2) - 1;


	double under_sqrt = (b * b) - (4 * a * c);


	if(under_sqrt == 0)
	{
		double i = 0.5 * (-b) / a;

		if (i > 0)
		{
			math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i * rayTransformed.get_direction() );
			double l = math3d::length (pNeu - ry.get_origin());		
			math3d::point pTest = (rayTransformed.get_origin() + i * rayTransformed.get_direction() );
			if (l < t && pTest[math3d::point::y] < 1 && pTest[math3d::point::y] >= 0)
			{
				p = pNeu;
				t = l;
				math3d::point pp(rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction());
			
				if ( math3d::length (pp - math3d::point(0,pp[math3d::point::y],0)) > (1 - pp[math3d::point::y])  )
					inout = 1;
				else 
					inout = -1;
			}
		}

	}
	else if (under_sqrt > 0)
	{
		double i_1 = 0.5 * (-b - sqrt(under_sqrt)) / a;
		double i_2 = 0.5 * (-b + sqrt(under_sqrt)) / a;

		if (i_1 > 0 && i_1 < i_2)
		{
			math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i_1 * rayTransformed.get_direction() );
			double l = math3d::length (pNeu - ry.get_origin());		
			math3d::point pTest = (rayTransformed.get_origin() + i_1 * rayTransformed.get_direction() );
			if (l < t && pTest[math3d::point::y] < 1 && pTest[math3d::point::y] >= 0)
			{
				p = pNeu;
				t = l;
				math3d::point pp(rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction());
			
				if ( math3d::length (pp - math3d::point(0,pp[math3d::point::y],0)) > (1 - pp[math3d::point::y])  )
					inout = 1;
				else 
					inout = -1;
			}	

		}
		else if (i_2 > 0)
		{
			math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i_2 * rayTransformed.get_direction() );
			double l = math3d::length (pNeu - ry.get_origin());		
			math3d::point pTest = (rayTransformed.get_origin() + i_2 * rayTransformed.get_direction() );
			if (l < t && pTest[math3d::point::y] < 1 && pTest[math3d::point::y] >= 0)
			{
				p = pNeu;
				t = l;
				math3d::point pp(rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction());
			
				if ( math3d::length (pp - math3d::point(0,pp[math3d::point::y],0)) > (1 - pp[math3d::point::y])  )
					inout = 1;
				else 
					inout = -1;
			}	

		}

	}

	if (t != talt)
	{
		p = getWorldMatrix() * (rayTransformed.get_origin() + t * rayTransformed.get_direction());

		if (inout == 1)
			return 1;
		else
			return -1;
	}
	else
		return 0;

}
//


//
// getNormal
//
math3d::vector	cone::getNormal(math3d::point const& p)	const
{
	//Punkt transformieren
	//Multiplikation mit inverser Matrix der cone
	math3d::point pTransformed (getWorldMatrixInvers() * p);

	if (std::abs(pTransformed[math3d::point::y]) <= 0.0001)//std::numeric_limits<double>::epsilon() )
		return math3d::normalize ( math3d::transpose(getWorldMatrixInvers()) * math3d::vector(0,-1,0) );
	else if (std::abs(pTransformed[math3d::point::y]) <= (height_ + 0.0001 ) &&//std::numeric_limits<double>::epsilon() )&&
		 std::abs(pTransformed[math3d::point::y]) >= (height_ - 0.0001 ) ) //std::numeric_limits<double>::epsilon() ) )
		return math3d::normalize ( math3d::transpose(getWorldMatrixInvers()) * math3d::vector(0,1,0) );
	else
	{
	return math3d::normalize ( math3d::transpose(getWorldMatrixInvers()) * math3d::vector(pTransformed[math3d::point::x], 
													       0.5,
												  pTransformed[math3d::point::z]) );
	
	}
		
}


math3d::vector	cone::getNormal(math3d::point const& p , double o)	const
{
	//Punkt transformieren
	//Multiplikation mit inverser Matrix der cone
	math3d::point pTransformed (getWorldMatrixInvers() * p);

		return math3d::normalize ( math3d::transpose(getWorldMatrixInvers()) * math3d::vector(0,-1,0) );
}







