#include "scene.hpp"
#include<iostream>
#include<cstdlib>


scene::scene(std::string const& s)

{
	std::ifstream infile;
	std::string str;

	infile.open(s.c_str(),std::ifstream::in);
	
	if(!infile.good())
	{
		std::cout<<"Datei nicht gefunden! Abbruch."<<std::endl;
		exit(1);
	}

	while(!infile.eof())
	{
		std::getline(infile,str);
		std::stringstream sstr(str);
		std::string k;
		double d;
		sstr>>k;
			
		if(k[0]=='#')
		{
			continue;
		}
		
		else
		{
			if(k=="define")
			{				
				sstr>>k;
				if(k=="shape")
				{
					sstr>>k;
					if(k=="box")
					{
						sstr>>k;
						std::string bname =k;
						

						math3d::point p1, p2;
						material a;

						sstr>>d;
						p1[math3d::point::x]=d;
						sstr>>d;
						p1[math3d::point::y]=d;
						sstr>>d;
						p1[math3d::point::z]=d;
						
						sstr>>d;
						p2[math3d::point::x]=d;
						sstr>>d;
						p2[math3d::point::y]=d;
						sstr>>d;
						p2[math3d::point::z]=d;

						sstr>>k;					
						std::string mat_name=k;
						
						for(std::list<material>::iterator i=mats_.begin();i!=mats_.end();++i)
						{
							if((*i).name_ == k)
							{
								a=(*i);
							}
						}
						
						shape *b = new box(p1,p2,bname,a);
						objects_.push_back(b);
					}				

					else if(k=="sphere")
					{
						sstr>>k;
						std::string sname=k;
						
						math3d::point center;
						material a;

						sstr>>d;
						center[math3d::point::x]=d;
						sstr>>d;
						center[math3d::point::y]=d;
						sstr>>d;
						center[math3d::point::z]=d;

						sstr>>d;						
						double radius=d;

						sstr>>k;
						std::string mat_name=k;
								
						for(std::list<material>::iterator i=mats_.begin();i!=mats_.end();++i)
						{
							if((*i).name_ == k)
							{
								a=(*i);
							}
						}
						shape *s = new sphere(center,radius,sname,a);
						objects_.push_back(s);
					}
					else if(k=="cone")
					{
						sstr>>k;
						std::string sname=k;
						
						math3d::point center;
						material a;

						sstr>>d;

						center[math3d::point::x]=d;
						sstr>>d;
						center[math3d::point::y]=d;
						sstr>>d;
						center[math3d::point::z]=d;

						sstr>>d;
						double radius = d;
						
						sstr>>d;
						double height = d;

						sstr>>k;
						std::string mat_name=k;
								
						for(std::list<material>::iterator i=mats_.begin();i!=mats_.end();++i)
						{
							if((*i).name_ == k)
							{
								a=(*i);
							}
						}
						shape *s = new cone(center,height, radius,sname,a);
						objects_.push_back(s);
					}
					else if(k=="cylinder")
					{
						sstr>>k;
						std::string sname=k;
						
						math3d::point center;
						material a;

						sstr>>d;
						center[math3d::point::x]=d;
						sstr>>d;
						center[math3d::point::y]=d;
						sstr>>d;
						center[math3d::point::z]=d;

						sstr>>d;
						double radius = d;

						sstr>>d;						
						double height = d;
						


						sstr>>k;
						std::string mat_name=k;
								
						for(std::list<material>::iterator i=mats_.begin();i!=mats_.end();++i)
						{
							if((*i).name_ == k)
							{
								a=(*i);
							}
						}
						shape *s = new cylinder(center, radius, height, sname,a);
						objects_.push_back(s);
					}
					else if(k=="triangle")
					{
						sstr>>k;
						std::string sname=k;
						
						math3d::point p_0,p_1,p_2;
						material a;

						sstr>>d;
						p_0[math3d::point::x]=d;
						sstr>>d;
						p_0[math3d::point::y]=d;
						sstr>>d;
						p_0[math3d::point::z]=d;

						sstr>>d;
						p_1[math3d::point::x]=d;
						sstr>>d;
						p_1[math3d::point::y]=d;
						sstr>>d;
						p_1[math3d::point::z]=d;

						sstr>>d;
						p_2[math3d::point::x]=d;
						sstr>>d;
						p_2[math3d::point::y]=d;
						sstr>>d;
						p_2[math3d::point::z]=d;


						sstr>>k;
						std::string mat_name=k;
								
						for(std::list<material>::iterator i=mats_.begin();i!=mats_.end();++i)
						{
							if((*i).name_ == k)
							{
								a=(*i);
							}
						}
						shape *s = new triangle(p_0, p_1,p_2,sname,a);
						objects_.push_back(s);
					}
				}
				else if(k=="light")
				{
					sstr>>k;
					std::string lname=k;
					math3d::point pos;
					
					sstr>>d;
					pos[math3d::point::x]=d;
					sstr>>d;
					pos[math3d::point::y]=d;
					sstr>>d;
					pos[math3d::point::z]=d;
					
					rgb la, ld;
					
					sstr>>d;
					la[rgb::r]=d;
					sstr>>d;
					la[rgb::g]=d;
					sstr>>d;
					la[rgb::b]=d;


					sstr>>d;
					ld[rgb::r]=d;
					sstr>>d;
					ld[rgb::g]=d;
					sstr>>d;
					ld[rgb::b]=d;


					light *s = new light(pos,lname,la,ld);
					
					lights_.push_back(*s);


				}
				else if(k=="material")
				{
					sstr>>k;
					std::string mname=k;
					rgb a, b, c, e;
					double f, g;
					
					sstr>>d;
					a[rgb::r]=d;
					sstr>>d;
					a[rgb::g]=d;
					sstr>>d;
					a[rgb::b]=d;
					
					sstr>>d;
					b[rgb::r]=d;
					sstr>>d;
					b[rgb::g]=d;
					sstr>>d;
					b[rgb::b]=d;
					

					sstr>>d;
					c[rgb::r]=d;
					sstr>>d;
					c[rgb::g]=d;
					sstr>>d;
					c[rgb::b]=d;

					sstr>>d;
					f=d;

					sstr>>d;
					e[rgb::r]=d;
					sstr>>d;
					e[rgb::g]=d;
					sstr>>d;
					e[rgb::b]=d;

					sstr >> d;
					g = d;
					
					material *m = new material(mname,a,b,c,f, e, g);   
					
					mats_.push_back(*m);


				}
				else if(k=="camera")
				{
					sstr>>k;
					std::string cname=k;
				

					sstr>>d;			
					double fx=d;
	
								
					camera *c = new camera(cname,fx);
					cams_.push_back(*c);
				}
			}

			else if(k=="transform")
			{
				sstr>>k;
				std::string shape_name=k;
				sstr>>k;
				
				if(k=="translate")
				{
					sstr>>d;
					double x_vec =d;
					sstr>>d;
					double y_vec =d;
					sstr>>d;
					double z_vec =d;
					math3d::vector trans(x_vec,y_vec,z_vec);
										

					for(std::list<shape*>::iterator i=objects_.begin();i!=objects_.end();++i)
					{
						if(shape_name==((*(*i)).getName()))
						(*(*i)).translate(trans);
					}
					for(std::list<camera>::iterator i = cams_.begin();i != cams_.end();++i)
					{
						if(shape_name == (*i).get_name())
							(*i).translate(trans);
					}
				}
				else if( k == "rotate" )
				{
					sstr >> d;
					double angle = d;
					sstr >> d;
					double xVec = d;
					sstr >> d;
					double yVec = d;
					sstr >> d;
					double zVec = d;
					math3d::vector rot(xVec,yVec,zVec);		

					for( std::list<shape*>::iterator i = objects_.begin(); i != objects_.end(); ++i )
					{
						if (shape_name == (**i).getName())
							(**i).rotate(rot, angle);
					}
					for(std::list<camera>::iterator i = cams_.begin(); i != cams_.end();++i)
					{
						if(shape_name == (*i).get_name())
							(*i).rotate(rot,angle);
					}
				}
				else if( k == "scale" )
				{
					sstr >> d;
					double xVec = d;
					sstr >> d;
					double yVec = d;
					sstr >> d;
					double zVec = d;
					math3d::vector scal(xVec,yVec,zVec);		

					for( std::list<shape*>::iterator i = objects_.begin(); i != objects_.end(); ++i )
					{
						if (shape_name == (**i).getName())
							(**i).scale(scal);
					}
					for(std::list<camera>::iterator i = cams_.begin();i != cams_.end();++i)
					{
						if(shape_name == (*i).get_name())
							(*i).scale(scal);
					}
				}
			}
			else if(k=="render")
			{
				sstr>>k;
				std::string cam_name=k;

				for(std::list<camera>::iterator i=cams_.begin();i!=cams_.end();++i)
				{
					if((*i).get_name()==k)
					{
						cam_=(*i);
					}
				}
				sstr>>k;				
				filename_=k;				
				
				sstr>>d;				
				x_res_=d;
				sstr>>d;				
				y_res_=d;
	
				sstr>>d;
				antialiasing_=d;
			}

		}
	
	}

	std::cout<<"\nBildname: "<<filename_<<std::endl;
	std::cout<<"Auflösung: "<<x_res_<<"x"<<y_res_<<std::endl;
	int k=0;	
	for(std::list<shape*>::iterator i=objects_.begin();i!=objects_.end();++i)
	{
		++k;
	}
	std::cout<<"Anzahl der Objekte: "<<k<<std::endl;
	if(antialiasing_)
	{
		std::cout<<"Antialiasing: ja"<<std::endl;
	}
	else
	{	
		std::cout<<"Antialiasing: nein"<<std::endl;
	}
	std::cout<<"\nArbeite..."<<std::endl;
}


scene::~scene()
  {}


