#include "box.hpp"

//
// Konstruktoren, Destruktor
//
box::box()
  {}

box::box(math3d::point const& p1, math3d::point const& p2, std::string const& name, material const& mat, math3d::matrix const& matr, math3d::matrix const& matrInv) : 
		shape(name,mat, matr, matrInv) , 
		vertexLuh_ (p1), 
		vertexRov_ (p2)		
	{
		scale (math3d::vector (std::abs (vertexLuh_[math3d::point::x] - vertexRov_[math3d::point::x]),
				       std::abs (vertexLuh_[math3d::point::y] - vertexRov_[math3d::point::y]),
				       std::abs (vertexLuh_[math3d::point::z] - vertexRov_[math3d::point::z]) ) );
		translate (math3d::vector(vertexLuh_[math3d::point::x], vertexLuh_[math3d::point::y], vertexLuh_[math3d::point::z]));

		vertexLuh_ = math3d::point (0,0,0);
		vertexRov_ = math3d::point (1,1,1);
	}



box::box(box const& bx) : 
		shape(bx) , 
		vertexLuh_(bx.vertexLuh_) , 
		vertexRov_(bx.vertexRov_)	
  {}

box::~box() 	
  {}


//
// volume - Berechnung boxvolumen
//
double	box::volume()	const
{
	return  std::abs( (vertexLuh_[math3d::point::x]-vertexRov_[math3d::point::x]) * 
			  (vertexLuh_[math3d::point::y]-vertexRov_[math3d::point::y]) * 
			  (vertexLuh_[math3d::point::z]-vertexRov_[math3d::point::z])   );
}
//


//
// isInside - Kollision
//
bool	box::isInside(math3d::point const& p)	const
{
	return ( (p[math3d::point::x] >= vertexLuh_[math3d::point::x] && p[math3d::point::x] <= vertexRov_[math3d::point::x]) &&
		 (p[math3d::point::y] >= vertexLuh_[math3d::point::y] && p[math3d::point::y] <= vertexRov_[math3d::point::y]) &&
		 (p[math3d::point::z] >= vertexLuh_[math3d::point::z] && p[math3d::point::z] <= vertexRov_[math3d::point::z]) );
}
//


//
// surface - Berechnung Oberfläche
//
double	box::surface()	const
{
     return (  std::abs(2* (vertexLuh_[math3d::point::x]-vertexRov_[math3d::point::x]) * (vertexLuh_[math3d::point::y]-vertexRov_[math3d::point::y])) 
	     + std::abs(2* (vertexLuh_[math3d::point::x]-vertexRov_[math3d::point::x]) * (vertexLuh_[math3d::point::z]-vertexRov_[math3d::point::z])) 
	     + std::abs(2* (vertexLuh_[math3d::point::z]-vertexRov_[math3d::point::z]) * (vertexLuh_[math3d::point::y]-vertexRov_[math3d::point::y])) );
}
//


//
// printOn - Ausgabe
//
void box::printOn(std::ostream& stream)		const
{
	stream << "\nBox: \n";
	shape::printOn(stream);
	stream << "Vertex Links unten hinten: " << vertexLuh_[math3d::point::x] << " " 
		<< vertexLuh_[math3d::point::y] << " " << vertexLuh_[math3d::point::z] << "\n";
	stream << "Vertex Rechts oben vorne: " << vertexRov_[math3d::point::x] << " " 
		<< vertexRov_[math3d::point::y] << " " << vertexRov_[math3d::point::z] << "\n";
	//stream << "Länge: " << x_ << " ,  Breite: " << z_ << " ,  Höhe: " << y_ << "\n";
	stream << "Volumen: " << volume() << "\nOberfläche: " << surface() << "\n";
}
//


//
// clone
//
box* box::clone()	const
{
	return new box(*this);	
}
//


//
// intersection
//
int box::intersection(ray const& ry, double & t, math3d::point & p) const
{
	//Strahl transformieren
	//Multiplikation mit inverser Matrix der Box
	ray rayTransformed = ray(getWorldMatrixInvers()  * ry.get_origin() , getWorldMatrixInvers()  * ry.get_direction() );

	double talt = 9999999;

	double p_x = 0, p_y = 0, p_z = 0;
	
	//linke seite quader
	double t_x_min = (-rayTransformed.get_origin()[math3d::point::x]) / rayTransformed.get_direction()[math3d::vector::x];

	if (t_x_min < talt && t_x_min > 0)
	{
		p_y = rayTransformed.get_origin()[math3d::point::y] + t_x_min * rayTransformed.get_direction()[math3d::vector::y];
		p_z = rayTransformed.get_origin()[math3d::point::z] + t_x_min * rayTransformed.get_direction()[math3d::vector::z];

		if( p_y >= 0 && p_y <= 1 &&
		    p_z >= 0 && p_z <= 1 )
			talt = t_x_min;
	}

	//untere seite quader
	double t_y_min = (-rayTransformed.get_origin()[math3d::point::y]) / rayTransformed.get_direction()[math3d::vector::y];

	if (t_y_min < talt && t_y_min > 0)
	{
		p_x = rayTransformed.get_origin()[math3d::point::x] + t_y_min * rayTransformed.get_direction()[math3d::vector::x];
		p_z = rayTransformed.get_origin()[math3d::point::z] + t_y_min * rayTransformed.get_direction()[math3d::vector::z];

		if( p_x >= 0 && p_x <= 1 &&
		    p_z >= 0 && p_z <= 1 )
			talt = t_y_min;
	}

	//hintere seite quader
	double t_z_min = (-rayTransformed.get_origin()[math3d::point::z]) / rayTransformed.get_direction()[math3d::vector::z];

	if (t_z_min < talt  && t_z_min > 0)
	{
		p_x = rayTransformed.get_origin()[math3d::point::x] + t_z_min * rayTransformed.get_direction()[math3d::vector::x];
		p_y = rayTransformed.get_origin()[math3d::point::y] + t_z_min * rayTransformed.get_direction()[math3d::vector::y];

		if( p_x >= 0 && p_x <= 1 &&
		    p_y >= 0 && p_y <= 1 )
			talt = t_z_min;
	}

	//rechte seite quader
	double t_x_max = (1 - rayTransformed.get_origin()[math3d::point::x]) / rayTransformed.get_direction()[math3d::vector::x];

	if (t_x_max < talt && t_x_max > 0)
	{
		p_y = rayTransformed.get_origin()[math3d::point::y] + t_x_max * rayTransformed.get_direction()[math3d::vector::y];
		p_z = rayTransformed.get_origin()[math3d::point::z] + t_x_max * rayTransformed.get_direction()[math3d::vector::z];

		if( p_y >= 0 && p_y <= 1 &&
		    p_z >= 0 && p_z <= 1 )
			talt = t_x_max;
	}

	//obere seite quader
	double t_y_max = (1 - rayTransformed.get_origin()[math3d::point::y]) / rayTransformed.get_direction()[math3d::vector::y];

	if (t_y_max < talt && t_y_max > 0)
	{
		p_x = rayTransformed.get_origin()[math3d::point::x] + t_y_max * rayTransformed.get_direction()[math3d::vector::x];
		p_z = rayTransformed.get_origin()[math3d::point::z] + t_y_max * rayTransformed.get_direction()[math3d::vector::z];

		if( p_x >= 0 && p_x <= 1 &&
		    p_z >= 0 && p_z <= 1 )
			talt = t_y_max;
	}

	//vordere seite quader
	double t_z_max = (1 - rayTransformed.get_origin()[math3d::point::z]) / rayTransformed.get_direction()[math3d::vector::z];

	if (t_z_max < talt && t_z_max > 0)
	{
		p_x = rayTransformed.get_origin()[math3d::point::x] + t_z_max * rayTransformed.get_direction()[math3d::vector::x];
		p_y = rayTransformed.get_origin()[math3d::point::y] + t_z_max * rayTransformed.get_direction()[math3d::vector::y];

		if( p_x >= 0 && p_x <= 1 &&
		    p_y >= 0 && p_y <= 1 )
			talt = t_z_max;
	}

	if (talt != 9999999)
	{
		math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + talt * rayTransformed.get_direction() );
		
		double l = math3d::length (pNeu - ry.get_origin());

		math3d::point pTest = (rayTransformed.get_origin() + 0.5 * talt * rayTransformed.get_direction() );

		if( l < t && 
		    pTest[math3d::point::x] > 0 && pTest[math3d::point::x] < 1 &&
		    pTest[math3d::point::y] > 0 && pTest[math3d::point::y] < 1 &&
		    pTest[math3d::point::z] > 0 && pTest[math3d::point::z] < 1     )
		{
			t = l;
			p = pNeu;
			return -1;		
		}
		else if (l < t)
		{
			p = pNeu;
			t = l;
			return 1;
		}
		else
			return 0;
	}
	else
		return 0;
}


//
// getNormal
//
math3d::vector	box::getNormal(math3d::point const& p)	const
{
	math3d::point pTransformed (getWorldMatrixInvers() * p);

	if (pTransformed[math3d::point::x] >= -0.00000001 && pTransformed[math3d::point::x] <= 0.00000001)
		return math3d::normalize (math3d::transpose(getWorldMatrixInvers()) * math3d::vector (-1,0,0) );

	else if (pTransformed[math3d::point::y] >= -0.00000001 && pTransformed[math3d::point::y] <= 0.00000001)
		return math3d::normalize (math3d::transpose(getWorldMatrixInvers()) * math3d::vector (0,-1,0) );

	else if (pTransformed[math3d::point::z] >= -0.00000001 && pTransformed[math3d::point::z] <= 0.00000001)
		return math3d::normalize (math3d::transpose(getWorldMatrixInvers()) * math3d::vector (0,0,-1) );

	else if (pTransformed[math3d::point::x] >= 0.99999999 && pTransformed[math3d::point::x] <= 1.00000001)
		return math3d::normalize (math3d::transpose(getWorldMatrixInvers()) * math3d::vector (1,0,0) );

	else if (pTransformed[math3d::point::y] >= 0.99999999 && pTransformed[math3d::point::y] <= 1.00000001)
		return math3d::normalize (math3d::transpose(getWorldMatrixInvers() ) * math3d::vector (0,1,0) );

	else if (pTransformed[math3d::point::z] >= 0.99999999 && pTransformed[math3d::point::z] <= 1.00000001)
		return math3d::normalize (math3d::transpose(getWorldMatrixInvers()) * math3d::vector (0,0,1) );

	else
	{
		std::cout << "Fehler Box Normale" << std::endl;
		return math3d::normalize(  math3d::transpose(getWorldMatrixInvers()) * math3d::vector (0,0,1) );	
	}
}





