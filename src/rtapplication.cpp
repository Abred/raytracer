#include "rtapplication.hpp"

rtApplication::rtApplication( )		
  {}

rtApplication::rtApplication(std::list<shape*> const& sl, camera const& cam, std::list<light> const& ll) :
		shapeList_ (sl) ,
		cam_(cam) ,
		lightList_(ll)	
  {}

rtApplication::rtApplication(scene const& sc) :
		shapeList_ (sc.objects_),
		cam_(sc.cam_),
		lightList_(sc.lights_),
		antia_(sc.antialiasing_),
		picname_(sc.filename_)
  {}

rtApplication::~rtApplication() 	
  {}

void 	rtApplication::raytrace() 	const
{
	// get glutwindow instance
   	glutwindow& gw = glutwindow::instance();
    
    	// create a ppmwriter
    	ppmwriter pw( gw.width(), gw.height(), "./"+picname_+".ppm");

	// Stoppuhr
  	timeval start, end;
  	gettimeofday(&start, 0);


    	// for all pixels of window
	double height (gw.height());
	double width (gw.width());
	double z = -0.5 / std::tan ( (cam_.get_angle()*0.5 * M_PI) / 180);



    	for (double y = 0; y < 0.5*(height/width); y += 1/width)
	{
      		for (double x = -0.5; x <=0; x += 1/width )
		{     
			// create pixel at x,y
		        pixel p((x+0.5)*width, (y+0.5*(height/width))*width);

			if(antia_)
			{
				for (double w = - 0.5/width; w <= 0.5/width; w += 0.5/width ) 
					for (double v = -0.5/width; v <= 0.5/width; v += 0.5/width )
					{
        					// compute color for pixel
						ray r(cam_.getWorldMatrix()*(math3d::point(0,0,0)), cam_.getWorldMatrix()*math3d::vector(x+w, y+v, z));
			
						int count = 0;

						rgb clr =  rraytrace(r, count);	
	
						p.color += rgb ( clr[rgb::r]/9, clr[rgb::g]/9, clr[rgb::b]/9 );		
					}
			}
			else
			{

				int count = 0;
				ray r(cam_.getWorldMatrix()*(math3d::point(0,0,0)), cam_.getWorldMatrix()*math3d::vector(x, y, z));
				p.color = rraytrace(r, count);
			}

       			// write pixel to output window
        		gw.write(p);

        		// write pixel to image writer
        		pw.write(p);
      		}
    	}

    	// save final image
    	pw.save();
	std::cout << "...Fertig!" << std::endl;

  	gettimeofday(&end, 0);
	int min = (end.tv_sec-start.tv_sec)/60;
	int sek = (end.tv_sec-start.tv_sec)%60;
  	std::cout <<"Benötigte Zeit: "<<min<<":"<<sek<<std::endl;
}

void 	rtApplication::raytrace1() 	const
{
	// get glutwindow instance
   	glutwindow& gw = glutwindow::instance();
    
    	// create a ppmwriter
    	ppmwriter pw( gw.width(), gw.height(), "./"+picname_+".ppm");

	// Stoppuhr
  	timeval start, end;
  	gettimeofday(&start, 0);


    	// for all pixels of window
	double height (gw.height());
	double width (gw.width());
	double z = -0.5 / std::tan ( (cam_.get_angle()*0.5 * M_PI) / 180);



    	for (double y = -0.5*(height/width); y <= 0; y += 1/width)
	{
      		for (double x = 0; x < 0.5; x += 1/width )
		{     
			// create pixel at x,y
		        pixel p((x+0.5)*width, (y+0.5*(height/width))*width);

			if(antia_)
			{
				for (double w = - 0.5/width; w <= 0.5/width; w += 0.5/width ) 
					for (double v = -0.5/width; v <= 0.5/width; v += 0.5/width )
					{
        					// compute color for pixel
						ray r(cam_.getWorldMatrix()*(math3d::point(0,0,0)), cam_.getWorldMatrix()*math3d::vector(x+w, y+v, z));
			
						int count = 0;

						rgb clr =  rraytrace(r, count);	
	
						p.color += rgb ( clr[rgb::r]/9, clr[rgb::g]/9, clr[rgb::b]/9 );		
					}
			}
			else
			{

				int count = 0;
				ray r(cam_.getWorldMatrix()*(math3d::point(0,0,0)), cam_.getWorldMatrix()*math3d::vector(x, y, z));
				p.color = rraytrace(r, count);
			}

       			// write pixel to output window
        		gw.write(p);

        		// write pixel to image writer
        		pw.write(p);
      		}
    	}

    	// save final image
    	pw.save();
	std::cout << "...Fertig!" << std::endl;

  	gettimeofday(&end, 0);
	int min = (end.tv_sec-start.tv_sec)/60;
	int sek = (end.tv_sec-start.tv_sec)%60;
  	std::cout <<"Benötigte Zeit: "<<min<<":"<<sek<<std::endl;
}

void 	rtApplication::raytrace2() 	const
{
	// get glutwindow instance
   	glutwindow& gw = glutwindow::instance();
    
    	// create a ppmwriter
    	ppmwriter pw( gw.width(), gw.height(), "./"+picname_+".ppm");

	// Stoppuhr
  	timeval start, end;
  	gettimeofday(&start, 0);


    	// for all pixels of window
	double height (gw.height());
	double width (gw.width());
	double z = -0.5 / std::tan ( (cam_.get_angle()*0.5 * M_PI) / 180);



    	for (double y = -0.5*(height/width); y <= 0; y += 1/width)
	{
      		for (double x = -0.5; x <= 0; x += 1/width )
		{     
			// create pixel at x,y
		        pixel p((x+0.5)*width, (y+0.5*(height/width))*width);

			if(antia_)
			{
				for (double w = - 0.5/width; w <= 0.5/width; w += 0.5/width ) 
					for (double v = -0.5/width; v <= 0.5/width; v += 0.5/width )
					{
        					// compute color for pixel
						ray r(cam_.getWorldMatrix()*(math3d::point(0,0,0)), cam_.getWorldMatrix()*math3d::vector(x+w, y+v, z));
			
						int count = 0;

						rgb clr =  rraytrace(r, count);	
	
						p.color += rgb ( clr[rgb::r]/9, clr[rgb::g]/9, clr[rgb::b]/9 );		
					}
			}
			else
			{

				int count = 0;
				ray r(cam_.getWorldMatrix()*(math3d::point(0,0,0)), cam_.getWorldMatrix()*math3d::vector(x, y, z));
				p.color = rraytrace(r, count);
			}

       			// write pixel to output window
        		gw.write(p);

        		// write pixel to image writer
        		pw.write(p);
      		}
    	}

    	// save final image
    	pw.save();
	std::cout << "...Fertig!" << std::endl;

  	gettimeofday(&end, 0);
	int min = (end.tv_sec-start.tv_sec)/60;
	int sek = (end.tv_sec-start.tv_sec)%60;
  	std::cout <<"Benötigte Zeit: "<<min<<":"<<sek<<std::endl;
}
void 	rtApplication::raytrace3() 	const
{
	// get glutwindow instance
   	glutwindow& gw = glutwindow::instance();
    
    	// create a ppmwriter
    	ppmwriter pw( gw.width(), gw.height(), "./"+picname_+".ppm");

	// Stoppuhr
  	timeval start, end;
  	gettimeofday(&start, 0);


    	// for all pixels of window
	double height (gw.height());
	double width (gw.width());
	double z = -0.5 / std::tan ( (cam_.get_angle()*0.5 * M_PI) / 180);



    	for (double y = 0; y <= 0.5*(height/width); y += 1/width)
	{
      		for (double x = 0; x < 0.5; x += 1/width )
		{     
			// create pixel at x,y
		        pixel p((x+0.5)*width, (y+0.5*(height/width))*width);

			if(antia_)
			{
				for (double w = - 0.5/width; w <= 0.5/width; w += 0.5/width ) 
					for (double v = -0.5/width; v <= 0.5/width; v += 0.5/width )
					{
        					// compute color for pixel
						ray r(cam_.getWorldMatrix()*(math3d::point(0,0,0)), cam_.getWorldMatrix()*math3d::vector(x+w, y+v, z));
			
						int count = 0;

						rgb clr =  rraytrace(r, count);	
	
						p.color += rgb ( clr[rgb::r]/9, clr[rgb::g]/9, clr[rgb::b]/9 );		
					}
			}
			else
			{

				int count = 0;
				ray r(cam_.getWorldMatrix()*(math3d::point(0,0,0)), cam_.getWorldMatrix()*math3d::vector(x, y, z));
				p.color = rraytrace(r, count);
			}

       			// write pixel to output window
        		gw.write(p);

        		// write pixel to image writer
        		pw.write(p);
      		}
    	}

    	// save final image
    	pw.save();
	std::cout << "...Fertig!" << std::endl;

  	gettimeofday(&end, 0);
	int min = (end.tv_sec-start.tv_sec)/60;
	int sek = (end.tv_sec-start.tv_sec)%60;
  	std::cout <<"Benötigte Zeit: "<<min<<":"<<sek<<std::endl;
}



rgb	rtApplication::rraytrace (ray ry, int count)	const
{
	double t = 999999; // std::numeric_limits<double>::max()
	shape* closestObj = 0;
	math3d::point p;
	math3d::point pHit;
	int hit = 0;
	int res = 0;
	int depth = 5;


	for ( std::list<shape*>::const_iterator i = shapeList_.begin(); i != shapeList_.end(); ++i)
	{
		res = (**i).intersection(ry, t, p);
			
		if ( res )
		{ 	
			closestObj = *i;
			pHit = p;
			hit = res;
		}
	}

	rgb color;
	if (closestObj == 0)
		return rgb (0,0,0);
	
	
	rgb col;

	//reflection
	if (count < depth && ( (closestObj->getMaterial().reflexionSpecular_[rgb::r] != 0) ||
			       (closestObj->getMaterial().reflexionSpecular_[rgb::g] != 0) || 
			       (closestObj->getMaterial().reflexionSpecular_[rgb::b] != 0)    ) )
	{
		rgb clr;
		//r = i − 2( n ⋅ i)n
		math3d::vector n ( hit * closestObj->getNormal(pHit) );
		math3d::vector i ( ry.get_direction() );
		math3d::vector r ( i - ( 2 * math3d::dot ( n, i ) * n ) );

		ray ryReflected ( pHit + ( 0.1 * n), r );
		
		clr = rraytrace ( ryReflected, count+1);
		color += rgb (  (closestObj->getMaterial().reflexionSpecular_[rgb::r]) * clr[rgb::r] / (std::pow(2,count)),
		      		(closestObj->getMaterial().reflexionSpecular_[rgb::g]) * clr[rgb::g] / (std::pow(2,count)),
		      		(closestObj->getMaterial().reflexionSpecular_[rgb::b]) * clr[rgb::b] / (std::pow(2,count)) );
	}





	//refraction
	if (count < depth && ( (closestObj->getMaterial().refraction_[rgb::r] != 0) ||
			       (closestObj->getMaterial().refraction_[rgb::g] != 0) || 
			       (closestObj->getMaterial().refraction_[rgb::b] != 0)     ) )
	{
		//r = n1 / n2
		//w = −(i ⋅ n)r
		//k = 1 + ( w − r )( w + r )
		//t = ri + ( w − sqrt(k) ) n

		math3d::vector n = ( hit * closestObj->getNormal(pHit) );
		math3d::vector i =( ry.get_direction() );
		double r;
		if ( hit < 0 )
			r = closestObj->getMaterial().refractionIndex_;
		else
			r = 1 / closestObj->getMaterial().refractionIndex_;

	
		double k = 1 - ( r * r * (1 - (math3d::dot ( n,i )*math3d::dot ( n,i ))));

		if ( k >= 0)
		{
			rgb clr;
			math3d::vector l =(( r * i - ( r * math3d::dot ( n,i ) + std::sqrt(k)) * n));
			ray ryRefracted (pHit - (0.1 * n), l );
			clr = rraytrace( ryRefracted, count+1);
			color += rgb (  (closestObj->getMaterial().refraction_[rgb::r]) * clr[rgb::r] / (std::pow(2,count)),
		      			(closestObj->getMaterial().refraction_[rgb::g]) * clr[rgb::g] / (std::pow(2,count)),
		      			(closestObj->getMaterial().refraction_[rgb::b]) * clr[rgb::b] / (std::pow(2,count)) );
		}
	}


	 col = shade (ry, closestObj, pHit, hit);
	color += col;

	return color;
}



rgb 	rtApplication::shade (ray ry, shape* closestObj, math3d::point pHit, int hit)	const
{

		//double red = 0, green = 0, blue = 0;
		double redAmbient   = 0, redDiffuse   = 0, redSpecular   = 0;
		double greenAmbient = 0, greenDiffuse = 0, greenSpecular = 0;
		double blueAmbient  = 0, blueDiffuse  = 0, blueSpecular  = 0;
			
		for (std::list<light>::const_iterator j = lightList_.begin(); j != lightList_.end(); ++j )
		{
			//ambient
			redAmbient +=   (*j).lightAmbient_[rgb::r] * closestObj->getMaterial().reflexionAmbient_[rgb::r];
			greenAmbient += (*j).lightAmbient_[rgb::g] * closestObj->getMaterial().reflexionAmbient_[rgb::g];
			blueAmbient +=  (*j).lightAmbient_[rgb::b] * closestObj->getMaterial().reflexionAmbient_[rgb::b];


			//shadow
			math3d::vector n (  hit * closestObj->getNormal(pHit) );

			double lightVisibleDif = 1;
			double lightVisibleSpec = 1;

			ray rayShadow ( pHit + ( 0.1 * n) , (*j).location_ - pHit );

			double tShadow = math3d::length ( (*j).location_ - pHit);

			for (std::list<shape*>::const_iterator k = shapeList_.begin(); k != shapeList_.end(); ++k )
			{		
					math3d::point p;		
					int res = (**k).intersection( rayShadow, tShadow, p);

					if ( res != 0 )
					{ 
						lightVisibleDif = 0;
						lightVisibleSpec = 0;
						break;
					}
			}


			//diffuse
			math3d::vector l ( math3d::normalize ( (*j).location_ - pHit) );
			//math3d::vector n -> siehe oben

			double cosinusDiffuse = math3d::dot ( l, n );

			if (cosinusDiffuse < 0 )
				lightVisibleDif = 0;

			if (lightVisibleDif == 1)
			{
				redDiffuse +=  cosinusDiffuse * (*j).lightDiffuse_[rgb::r] * closestObj->getMaterial().reflexionDiffuse_[rgb::r];
				greenDiffuse+= cosinusDiffuse * (*j).lightDiffuse_[rgb::g] * closestObj->getMaterial().reflexionDiffuse_[rgb::g];	
				blueDiffuse += cosinusDiffuse * (*j).lightDiffuse_[rgb::b] * closestObj->getMaterial().reflexionDiffuse_[rgb::b];
			}


			//specular

			//r = 2 (n*l) n - l

			//math3d::vector n -> siehe oben
			//math3d::vector l -> siehe oben
			math3d::vector r ( math3d::normalize ( (2 * math3d::dot ( n, l ) * n) - l ) );

			//(r*v)m
			//betrag ?
			double cosinusSpecular = math3d::dot( r, (-1)*ry.get_direction() );

			if (cosinusSpecular < 0 )
				lightVisibleSpec = 0;

			if (lightVisibleSpec == 1)
			{
		        	redSpecular +=   std::pow(cosinusSpecular,closestObj->getMaterial().expReflexionSpecular_) * closestObj->getMaterial().reflexionSpecular_[rgb::r];
				greenSpecular += std::pow(cosinusSpecular,closestObj->getMaterial().expReflexionSpecular_) * closestObj->getMaterial().reflexionSpecular_[rgb::g];
				blueSpecular +=  std::pow(cosinusSpecular,closestObj->getMaterial().expReflexionSpecular_) * closestObj->getMaterial().reflexionSpecular_[rgb::b];
			}				
		}
		return rgb ((redAmbient*redDiffuse)+redSpecular, (greenAmbient*greenDiffuse)+greenSpecular, (blueAmbient*blueDiffuse)+blueSpecular);
}














