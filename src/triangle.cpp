#include "triangle.hpp"


//Konstruktoren, Destruktor
triangle::triangle():
	p1_(math3d::point(0,0,0)),
	p2_(math3d::point(1,0,0)),
	p3_(math3d::point(0,1,0))
  {}

triangle::triangle(math3d::point const& p1, math3d::point const& p2, math3d::point const& p3, std::string const& name, material const& mat,
					math3d::matrix const& matr , math3d::matrix const& matrInv):
	shape(name,mat,matr,matrInv),
	p1_(p1),
	p2_(p2),
	p3_(p3)		
  {}



triangle::triangle(triangle const& tr):
	shape(tr),
	p1_(tr.p1_),
	p2_(tr.p2_),
	p3_(tr.p3_)
  {}


triangle::~triangle()
  {}

//Hier nicht benötigte Funktionen daher nur definiert um den pure-virtual-Status der Shape-Klasse zu verlassen
double	triangle::volume()	const	{ return 0; }
bool	triangle::isInside(math3d::point const& p)	const	{return 0;}
double	triangle::surface()	const	{return 0;}
void	triangle::printOn(std::ostream& stream)	const {}
triangle* triangle::clone()	const	{return new triangle(*this);}



//
//Intersection
//
int triangle::intersection(ray const& ry, double & t, math3d::point & p) const
{

	ray rayTransformed = ray(getWorldMatrixInvers()  * ry.get_origin() , getWorldMatrixInvers()  * ry.get_direction() );

	math3d::vector n = getNormal(getWorldMatrix() * p1_);
	double i = (-math3d::dot(rayTransformed.get_origin() - p1_, n)) / math3d::dot (rayTransformed.get_direction(), n);

	if (i > 0)
	{

		math3d::point pTest = rayTransformed.get_origin() + i * rayTransformed.get_direction();

//Lösung Gleichungssystem allgemeiner Geradenpunkt in Ebene eingesetzt
		double w = ((p2_[math3d::point::z]*p1_[math3d::point::y]*pTest[math3d::point::x]-p1_[math3d::point::x]*p2_[math3d::point::z]*pTest[math3d::point::y]+p1_[math3d::point::x]*p2_[math3d::point::y]*pTest[math3d::point::z]-p2_[math3d::point::y]*p1_[math3d::point::z]*pTest[math3d::point::x]-p1_[math3d::point::y]*p2_[math3d::point::x]*pTest[math3d::point::z]+p1_[math3d::point::z]*p2_[math3d::point::x]*pTest[math3d::point::y]) / (p1_[math3d::point::x]*p2_[math3d::point::y]*p3_[math3d::point::z]-p1_[math3d::point::x]*p2_[math3d::point::z]*p3_[math3d::point::y]-p1_[math3d::point::y]*p2_[math3d::point::x]*p3_[math3d::point::z]+p2_[math3d::point::z]*p1_[math3d::point::y]*p3_[math3d::point::x]-p2_[math3d::point::y]*p1_[math3d::point::z]*p3_[math3d::point::x]+p1_[math3d::point::z]*p2_[math3d::point::x]*p3_[math3d::point::y]) );

		double v = (-p1_[math3d::point::x]*p3_[math3d::point::y]*pTest[math3d::point::z]+p1_[math3d::point::x]*pTest[math3d::point::y]*p3_[math3d::point::z]-pTest[math3d::point::y]*p1_[math3d::point::z]*p3_[math3d::point::x]+p3_[math3d::point::y]*p1_[math3d::point::z]*pTest[math3d::point::x]+p1_[math3d::point::y]*p3_[math3d::point::x]*pTest[math3d::point::z]-p1_[math3d::point::y]*pTest[math3d::point::x]*p3_[math3d::point::z])/(p1_[math3d::point::x]*p2_[math3d::point::y]*p3_[math3d::point::z]-p1_[math3d::point::x]*p2_[math3d::point::z]*p3_[math3d::point::y]-p1_[math3d::point::y]*p2_[math3d::point::x]*p3_[math3d::point::z]+p2_[math3d::point::z]*p1_[math3d::point::y]*p3_[math3d::point::x]-p2_[math3d::point::y]*p1_[math3d::point::z]*p3_[math3d::point::x]+p1_[math3d::point::z]*p2_[math3d::point::x]*p3_[math3d::point::y]);

		double u = -(-p2_[math3d::point::x]*p3_[math3d::point::y]*pTest[math3d::point::z]+p2_[math3d::point::x]*pTest[math3d::point::y]*p3_[math3d::point::z]-p3_[math3d::point::x]*p2_[math3d::point::z]*pTest[math3d::point::y]+p3_[math3d::point::x]*p2_[math3d::point::y]*pTest[math3d::point::z]-pTest[math3d::point::x]*p2_[math3d::point::y]*p3_[math3d::point::z]+pTest[math3d::point::x]*p2_[math3d::point::z]*p3_[math3d::point::y])/(p1_[math3d::point::x]*p2_[math3d::point::y]*p3_[math3d::point::z]-p1_[math3d::point::x]*p2_[math3d::point::z]*p3_[math3d::point::y]-p1_[math3d::point::y]*p2_[math3d::point::x]*p3_[math3d::point::z]+p2_[math3d::point::z]*p1_[math3d::point::y]*p3_[math3d::point::x]-p2_[math3d::point::y]*p1_[math3d::point::z]*p3_[math3d::point::x]+p1_[math3d::point::z]*p2_[math3d::point::x]*p3_[math3d::point::y]);




		math3d::point pNeu = getWorldMatrix() * (rayTransformed.get_origin() + i * rayTransformed.get_direction());
		double l = math3d::length (pNeu - ry.get_origin());

		if (u >= 0 && v >=0 && w >= 0 && u+v+w <= 1.0001 && l < t)
		{
			double cos = math3d::dot(ry.get_direction(), n);

			p = pNeu;
			t = i;

			if (cos < 0)
				return 1;
			else
				return -1;
			
		}
		else
			return 0;	
	}
	else
		return 0;
	


}


//
//getNormal
//
math3d::vector	triangle::getNormal(math3d::point const& p)	const
{
	math3d::point pTransformed (getWorldMatrixInvers() * p);

	return math3d::normalize (math3d::transpose(getWorldMatrixInvers()) * math3d::cross (p2_ - p1_, p3_ - p1_));
}



	
