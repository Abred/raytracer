#include "camera.hpp"

//
// Konstruktoren
//
camera::camera()
  {}

camera::camera(std::string const& n, double x, math3d::matrix const& matr, math3d::matrix const& matrInv) : 
		name_(n) , 
		fovX_(x), 
		worldMatrix_(matr),
		worldMatrixInvers_(matrInv)	
  {}


camera::~camera()
  {}
//
// translate
//
void	camera::translate(math3d::vector const& v)
{
	worldMatrix_ *= translationmatrix(v);
	worldMatrixInvers_ = inverse(worldMatrix_);
}


//
// scale
//
void	camera::scale(math3d::vector const& v)
{
	worldMatrix_ *= scalingmatrix(v);
	worldMatrixInvers_ = inverse(worldMatrix_);
}



//
// rotate
//
void	camera::rotate(math3d::vector const& v, double w)
{
	worldMatrix_ *= rotationmatrix(v,w);
	worldMatrixInvers_ = inverse(worldMatrix_);
}

//
//getter
//


std::string camera::get_name()	const
{
	return name_;
}

double 	camera::get_angle()	const
{
	return fovX_;
}

math3d::matrix camera::getWorldMatrix()	const
{
	return worldMatrix_;
}


math3d::matrix camera::getWorldMatrixInvers()	const
{
	return worldMatrixInvers_;
}
