#include "cylinder.hpp"


cylinder::cylinder()	{}
cylinder::cylinder(math3d::point const& m ,double ra,double h, std::string const& name , material const& mat, 
					math3d::matrix const& matr,math3d::matrix const& matrInv):
			
					shape(name,mat,matr,matrInv),
					center_(m),
					radius_(ra),
					height_(h)	
//Zylinder "normalisieren"
{

	if(radius_ != 1)
	{
		scale(math3d::vector(radius_,radius_,1));
		radius_ = 1;
	}
	if(height_ != 1)
	{
		scale(math3d::vector(1,1,height_));
		height_ = 1;
	}

	if(center_[math3d::point::x] != 0 || center_[math3d::point::y] !=0 || center_[math3d::point::z] != 0)
	{
		translate(math3d::vector(center_[math3d::point::x], center_[math3d::point::y], center_[math3d::point::z]));
		center_ = math3d::point (0,0,0);
	} 


	
}

cylinder::cylinder(cylinder const& cy) :
				shape(cy),
				center_(cy.center_),
				radius_(cy.radius_),
				height_(cy.height_)
							{}

cylinder::~cylinder()	{}


//
//Volumenberechnung
//
double	cylinder::volume()	const
{
	return M_PI*radius_*radius_*height_; 
}


//
//IsInside (da hier nicht benötigt, nicht berechnet aber definiert damit die pure virtual Eigenschaft wegfällt)
//
bool	cylinder::isInside(math3d::point const& p)	const	
{return 0;}


//
//Oberflächenberechnung
//
double	cylinder::surface()	const	
{
	return 2*M_PI*radius_*(radius_+height_);
}

//
//Ausgabe
//
void	cylinder::printOn(std::ostream& stream)	const 
{
	stream << "\nCylinder:\n";
	shape::printOn(stream);
	stream << "Mittelpunkt: (" << center_[math3d::point::x] << "," << center_[math3d::point::y] << "," << center_[math3d::point::z] << ")\n";
	stream << "Radius: " << radius_ << "\n";
	stream << "Höhe: " << height_ << "\n";
}

//
//Klonen
//
cylinder* cylinder::clone()	const	
{
	return new cylinder(*this);
}


//
//Intersection
//
int cylinder::intersection(ray const& ry, double & t, math3d::point & p) const
{

	//Strahl transformieren
	//Multiplikation mit inverser Matrix des Zylinder
	ray rayTransformed = ray(getWorldMatrixInvers()*(ry.get_origin()),getWorldMatrixInvers()*ry.get_direction(), 1.0);

	int inout = 0;
	double talt = t;

	//Überprüfung der Deckflächen
	
	double i_3 = -rayTransformed.get_origin()[math3d::point::z] / rayTransformed.get_direction()[math3d::vector::z];

	if (i_3 > 0)
	{
		math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i_3 * rayTransformed.get_direction() );
		math3d::point pTest = rayTransformed.get_origin() + i_3 * rayTransformed.get_direction() ;
		double l = math3d::length (pNeu - ry.get_origin());		

		if (l < t && math3d::length(pTest - center_) < 1.0)
		{
			p = pNeu;
			t = i_3;
			math3d::point pp(rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction());
			if ( pp[math3d::point::z] < 0  )
				inout = 1;
			else 
				inout = -1;
		}	
	}
	double i_4 = ((1 - rayTransformed.get_origin()[math3d::point::z]) / rayTransformed.get_direction()[math3d::vector::z]);

	if (i_4 > 0)
	{
		math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i_4 * rayTransformed.get_direction() );
		math3d::point pTest = rayTransformed.get_origin() + i_4 * rayTransformed.get_direction() ;
		double l = math3d::length (pNeu - ry.get_origin());		

		if (l < t && math3d::length(pTest - math3d::point(0,0,1)) < 1.0)
		{
			p = pNeu;
			t = i_4;
			math3d::point pp(rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction());
			if ( pp[math3d::point::z] > 1 )
				inout = 1;
			else 
				inout = -1;
		}	
	}

	//Überprüfung des Mantels mittels der allgemeinen Lsg einer quadratischen Gleichung
	

	//v1^2 + v2^2
	double A = std::pow(rayTransformed.get_direction()[math3d::vector::x],2) + std::pow(rayTransformed.get_direction()[math3d::vector::y],2);

	//2*(v1*p1+v2*p2
	double B = 2 * (rayTransformed.get_direction()[math3d::vector::x] * rayTransformed.get_origin()[math3d::point::x] + rayTransformed.get_direction()[math3d::vector::y] * rayTransformed.get_origin()[math3d::point::y]);

	//p1*p1+p2*p2
	double C = rayTransformed.get_origin()[math3d::point::x] * rayTransformed.get_origin()[math3d::point::x] + rayTransformed.get_origin()[math3d::point::y] * rayTransformed.get_origin()[math3d::point::y] - 1;

	double underSqrt = B * B - 4 * A * C;

	if (underSqrt == 0)
	{
		double i = - B / (2 * A);

		if (i > 0)
		{
			math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i * rayTransformed.get_direction() );
			math3d::point pTest = rayTransformed.get_origin() + i * rayTransformed.get_direction() ;
			double l = math3d::length (pNeu - ry.get_origin());		

			if (l < t && pTest[math3d::point::z] < 1 && pTest[math3d::point::z] > 0)
			{
				p = pNeu;
				t = l;
				
				math3d::point pp(rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction());
				if ( std::abs (math3d::length ( math3d::point(0,0,pp[math3d::point::z]) - pp)) > radius_ )
					inout = 1;
				else 
					inout = -1;
			}	
		}
	}
	else if (underSqrt > 0)
	{
		double i_1 = 0.5 * (-B - std::sqrt(underSqrt)) / A;
		double i_2 = 0.5 * (-B + std::sqrt(underSqrt)) / A;

		if( i_1 <= i_2 && i_1 > 0)
		{
			math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i_1 * rayTransformed.get_direction() );
			math3d::point pTest = rayTransformed.get_origin() + i_1 * rayTransformed.get_direction() ;
			double l = math3d::length (pNeu - ry.get_origin());		

			if (l < t && pTest[math3d::point::z] < 1 && pTest[math3d::point::z] > 0)
			{
				p = pNeu;
				t = l;
				math3d::point pp(rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction());
				if ( std::abs (math3d::length ( math3d::point(0,0,pp[math3d::point::z]) - pp)) > radius_ )
					inout = 1;
				else 
					inout = -1;
			}
		}
		else if (i_2 > 0)
		{
			math3d::point pNeu =  getWorldMatrix() * (rayTransformed.get_origin() + i_2 * rayTransformed.get_direction() );
			math3d::point pTest = rayTransformed.get_origin() + i_2 * rayTransformed.get_direction() ;
			double l = math3d::length (pNeu - ry.get_origin());		

			if (l < t && pTest[math3d::point::z] < 1 && pTest[math3d::point::z] > 0)
			{
				p = pNeu;
				t = l;
				math3d::point pp(rayTransformed.get_origin() + 0.5 * t * rayTransformed.get_direction());
				if ( std::abs (math3d::length ( math3d::point(0,0,pp[math3d::point::z]) - pp)) > radius_ )
					inout = 1;
				else 
					inout = -1;
			}
	
		}

	}

	if (t != talt)
	{
		p = getWorldMatrix() * (rayTransformed.get_origin() + t * rayTransformed.get_direction());

		if (inout == 1)
			return 1;
		else
			return -1;
	}
	else
		return 0;


}


//
// getNormal
//
math3d::vector	cylinder::getNormal(math3d::point const& p)	const
{
	//Punkt transformieren
	//Multiplikation mit inverser Matrix des Zylinder
	math3d::point pTransformed (getWorldMatrixInvers() * p);

	if (pTransformed[math3d::point::z] == 1 || (pTransformed[math3d::point::z] > 0.9999 && pTransformed[math3d::point::z] < 1.0001))
		return math3d::normalize(  math3d::transpose(getWorldMatrixInvers()) * math3d::vector(0,0,1 ));
	if (pTransformed[math3d::point::z] == 0 || (pTransformed[math3d::point::z] > -0.0001 && pTransformed[math3d::point::z] < 0.0001))
		return math3d::normalize(  math3d::transpose(getWorldMatrixInvers()) * math3d::vector(0,0,-1 ));
	else
		return math3d::normalize(  math3d::transpose(getWorldMatrixInvers()) * (pTransformed - math3d::point(0,0,pTransformed[math3d::point::z])) );
}

math3d::vector	cylinder::getNormal(math3d::point const& p, double o)	const
{
	//Punkt transformieren
	//Multiplikation mit inverser Matrix des Zylinder
	math3d::point pTransformed (getWorldMatrixInvers() * p);

	return    (pTransformed-center_ );
}


