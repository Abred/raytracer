#include "ray.hpp"

ray::ray() :
	origin_(0,0,0,0),
	direction_(0,0,1,0)		
  {}

//Normalisierter Strahl
ray::ray(math3d::point o,math3d::vector d) :
  	origin_(o) ,
 	direction_(d)	

{
	//Normalisieren des Richtungsvektors

	double laenge_dir = std::sqrt(std::pow(direction_[math3d::vector::x],2) +
			              std::pow(direction_[math3d::vector::y],2) +
				      std::pow(direction_[math3d::vector::z],2)  );
	
	if(laenge_dir != 1)
	{
		direction_ = direction_/laenge_dir;
	}
}

//Nicht normalisierter Strahl (ohne Bool gelöst um ständige if-Abfrage zuvermeiden)
ray::ray(math3d::point o,math3d::vector d, double c) :
  	origin_(o) ,
 	direction_(d)	
  {}


math3d::point	ray::get_origin()	const
{
	return origin_;
}

math3d::vector	ray::get_direction()	const
{
	return direction_;
}

