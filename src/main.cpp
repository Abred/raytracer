#include "glutwindow.hpp"
#include "ppmwriter.hpp"
#include "pixel.hpp"
#include "vector.hpp"
#include "ray.hpp"
#include "camera.hpp"
#include "shape.hpp"
#include "sphere.hpp"
#include "box.hpp"
#include "light.hpp"
#include "rtapplication.hpp"
#include "scene.hpp"
#include "cone.hpp"
#include "cylinder.hpp"
#include "triangle.hpp"


#include <iostream>
#include <string>
#include <cmath>
#include <boost/thread/thread.hpp>
#include <boost/bind.hpp>

#ifdef __APPLE__
  #include <GLUT/glut.h>
#else
  #include <GL/glut.h>
#endif



int main(int argc, char* argv[])
{

	std::string s;
	if(argc>1)
	{		
		s=argv[1];
	}else
	{
		std::cout<<"Keine Datei angehängt. Starte Beispielbild"<<std::endl;
		s="example.sdf";
	}

	scene sc(s);
  	// set resolution
  	const std::size_t width = sc.x_res_;
  	const std::size_t height = sc.y_res_;
	
  	// create output window
  	glutwindow::init(width, height, 100, 100, sc.filename_, argc, argv);
 	
  	// create a ray tracing application
  	rtApplication app(sc);


	
  	// start computation in thread
  	boost::thread thr(boost::bind(&rtApplication::raytrace, &app));
  	boost::thread thr1(boost::bind(&rtApplication::raytrace1, &app));
  	boost::thread thr2(boost::bind(&rtApplication::raytrace2, &app));
  	boost::thread thr3(boost::bind(&rtApplication::raytrace3, &app));
  	
  	// start output on glutwindow
  	glutwindow::instance().run();
  	
  	// wait on thread
  	thr.join();
	
  	return 0;
}









