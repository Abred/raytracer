#include "shape.hpp"


//Konstruktoren, Destruktor
shape::shape() : refCount_(0)	
  {}


shape::shape(std::string const& s, material const& mat, math3d::matrix const& matr, math3d::matrix const& matrInv) : 
		name_(s) , 
		refCount_(0), 
		material_(mat),
		worldMatrix_(matr),
		worldMatrixInvers_(matrInv)	
  {}


shape::shape(shape const& sh) : 
		name_(sh.name_), 
		refCount_(0), 
		material_(sh.material_),
		worldMatrix_(sh.worldMatrix_),
		worldMatrixInvers_(sh.worldMatrixInvers_)	
  {}


shape::~shape()
  {}


//
// getmaterial
//
material shape::getMaterial()	const
{
	return material_;	
}
//


//
// getname
//
std::string shape::getName()	const
{
	return name_;	
}
//


//
// printOn
//
void shape::printOn( std::ostream& stream)	const
{
	stream << "Name: " << name_ << "\n";
}
//

std::ostream& operator<<(std::ostream& stream, shape const& sh)
{
	sh.printOn(stream);
	return stream;
}

bool operator==(shape const& lhs, shape const& rhs)
{
	return lhs.getName() == rhs.getName();	
}


//
// ref
//
int	shape::ref()
{
	return ++refCount_;
}
//


//
// unRef
//
int	shape::unRef()
{
	--refCount_;
	if (refCount_ <= 0)
	{
		delete this;	
		return 0;
	}
	return refCount_;
}
//


//
// translate
//
void	shape::translate(math3d::vector const& v)
{

	worldMatrix_ = translationmatrix(v) * worldMatrix_;
	worldMatrixInvers_ = math3d::inverse(worldMatrix_);
}
//


//
// scale
//
void	shape::scale(math3d::vector const& v)
{


	worldMatrix_ *= scalingmatrix(v);
	worldMatrixInvers_ = math3d::inverse(worldMatrix_);
}
//


//
// rotate
//
void	shape::rotate(math3d::vector const& v, double w)
{

	double da = worldMatrix_[math3d::matrix::da];
	double db = worldMatrix_[math3d::matrix::db];
	double dc = worldMatrix_[math3d::matrix::dc];

	worldMatrix_[math3d::matrix::da] = 0;
	worldMatrix_[math3d::matrix::db] = 0;
	worldMatrix_[math3d::matrix::dc] = 0;

	worldMatrix_ = rotationmatrix(v,w) * worldMatrix_;
	worldMatrix_[math3d::matrix::da] = da;
	worldMatrix_[math3d::matrix::db] = db;
	worldMatrix_[math3d::matrix::dc] = dc;


	worldMatrixInvers_ = inverse(worldMatrix_);
}
//


math3d::matrix const&	shape::getWorldMatrix()				const
{
	return worldMatrix_;	
}
math3d::matrix const& shape::getWorldMatrixInvers()				const
{
	return worldMatrixInvers_;	
}









